CREATE DATABASE practice DEFAULT CHARACTER SET utf8;
USE practice;
--1
CREATE TABLE item_category (
category_id int PRIMARY KEY AUTO_INCREMENT,
category_name varchar(256) NOT NULL);
--2
CREATE TABLE item (
item_id int PRIMARY KEY AUTO_INCREMENT,
item_name varchar(256) NOT NULL,
Item_price int NOT NULL,
category_id int 
);
--3
INSERT INTO item_category(category_name) VALUES ('家具');
INSERT INTO item_category(category_name) VALUES ('食品');
INSERT INTO item_category(category_name) VALUES ('本');
--4
INSERT INTO item(item_name,item_price,category_id) VALUES ('堅牢な机',3000,1);
INSERT INTO item(item_name,item_price,category_id) VALUES ('生焼け肉',50,2);
INSERT INTO item(item_name,item_price,category_id) VALUES ('すっきりわかるJava入門',3000,3);
INSERT INTO item(item_name,item_price,category_id) VALUES ('おしゃれな椅子',2000,1);
INSERT INTO item(item_name,item_price,category_id) VALUES ('こんがり肉',500,2);
INSERT INTO item(item_name,item_price,category_id) VALUES ('書き方ドリルSQL',2500,3);
INSERT INTO item(item_name,item_price,category_id) VALUES ('ふわふわのベッド',30000,1);
INSERT INTO item(item_name,item_price,category_id) VALUES ('ミラノ風ドリア',300,2);
--5
SELECT item_name,item_price  FROM item WHERE category_id = 1;
--6
SELECT item_name,item_price  FROM item WHERE item_price >= 1000;
--7
SELECT item_name,item_price  FROM item WHERE item_name LIKE '%肉%';
--8
SELECT
    item.item_id,
    item.item_name,
    item.Item_price,
    item_category.category_name
FROM
    item 
INNER JOIN
    item_category 
ON
    item.category_id = item_category.category_id;
--9
SELECT
    ic.category_name,
    SUM(i.Item_price)AS total_price
FROM
    item i
INNER JOIN
    item_category ic
ON
    i.category_id = ic.category_id
GROUP BY
    ic.category_id
ORDER BY
    total_price DESC;

