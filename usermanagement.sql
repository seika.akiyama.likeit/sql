CREATE DATABASE usermanagement DEFAULT CHARACTER SET utf8;
USE usermanagement;

CREATE TABLE user (
id SERIAL PRIMARY KEY AUTO_INCREMENT,
login_id varchar(255) UNIQUE NOT NULL,
name varchar(255) NOT NULL,
birth_date DATE NOT NULL,
password varchar(255)  NOT NULL,
is_admin boolean NOT NULL default 0,
create_date DATETIME NOT NULL,
update_date DATETIME NOT NULL);

INSERT INTO user(id,login_id,name,birth_date,password,is_admin,create_date,update_date) VALUES (1,'admin','�Ǘ���','1987-09-11','password',1,now(),now());


SELECT * FROM user WHERE login_id = 'admin'and password = 'password';  --ok
SELECT * FROM user WHERE login_id = 'admin'and password = 'hoge';  --�~
SELECT * FROM user WHERE login_id = ? and password = ?;

